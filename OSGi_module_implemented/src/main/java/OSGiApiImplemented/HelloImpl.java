package OSGiApiImplemented;

import DataTransfer.DataTransfer;
import OSGiApi.HelloAPI;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Implementacia rozhrania modulu. Je to singleton a tento singleton je mozne
 * injektnut do inych OSGi modulov. Ked nie je nasadeny ine moduly o tom automaticky
 * vedia a vieme na to regovat.
 * <p>
 * Je mozne injektovat medzi modulmi IBA beany z balicku java.ejb. Napriklad RequestScoped beana
 * medzi modulmi injektnut nejde.
 * <p>
 * To ze tato beana je pristupna ostatnym OSGi modulom dosiahneme pomocou "Export-EJB: ALL"
 * v manifeste tohto modulu. Je samozrejme mozne si vybrat iba niektore beany, nemusia to byt vsetky.
 * Ak tu idea podtrhava na cerveno to HelloAPI tak to ignorujte ona pinda ze som to nepridal do manifestu
 * ale manifest generuje automaticky ten maven plugin. To idea nevie.
 */

@Stateless
public class HelloImpl implements HelloAPI {
    @Inject
    TestovaciaBeana testovaciaBeana; //nejaka dalsia beana na ktoru to deleguje pre demonstraciu

    public String hello() {
        return testovaciaBeana.getHello();
    }

    @Override
    public DataTransfer transferHello() {
        return new DataTransfer(testovaciaBeana.getHello());
    }
}
