package OSGiApiImplemented;

import javax.enterprise.context.RequestScoped;

/**
 * Tato beana zije vo vnutri tohto OSGi modulu a z vonka nie je pristupna.
 */
@RequestScoped
public class TestovaciaBeana {
    public String getHello() {
        return "helllo";
    }
}
