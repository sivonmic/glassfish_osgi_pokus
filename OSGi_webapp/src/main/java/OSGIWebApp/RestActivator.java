package OSGIWebApp;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Zapneme rest na adrese /api.
 */
@ApplicationPath("/api")
public class RestActivator extends Application {
}
