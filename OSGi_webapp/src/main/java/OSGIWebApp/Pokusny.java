package OSGIWebApp;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Nejaky dalsi rest endpoint na testovanie ci to bezi.
 */
@RequestScoped
@Path("/")
public class Pokusny {
    @GET
    @Path("pokus")
    @Produces(MediaType.TEXT_PLAIN)
    public String pokus() {
        return "pokus";
    }
}
