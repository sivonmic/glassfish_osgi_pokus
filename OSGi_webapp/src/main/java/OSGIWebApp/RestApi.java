package OSGIWebApp;

import OSGiApi.HelloAPI;
import org.glassfish.osgicdi.OSGiService;
import org.glassfish.osgicdi.ServiceUnavailableException;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Rest api v hlavnej appke. Toto api si injektuje OSGi modul a deleguje nanho.
 * Vie kedy tento osgi modul nebezi a vie na to reagovat.
 * <p>
 * ADRESA: http://localhost:8080/osgi_hello/api/hello
 * ak mate inak nastaveny port na payare tak zmente tento port.
 */
@Singleton
@Path("/")
public class RestApi {
    int counter = 0;

    @Inject
    @OSGiService(dynamic = true) //injektneme interface OSGi modul. To dynamic = true je VELMI dolezite!
    private HelloAPI helloAPI;

    @GET
    @Path("hello")
    public String doHello() {
        try {
            return helloAPI.hello() + ++counter;
        } catch (ServiceUnavailableException e) { //ked modul nebezi dostaneme vynimku
            return "Service je dole";
        }
    }

    /**Demonstracia predania vysledku z modulu do modulu cez objekt */
    @GET
    @Path("transferHello")
    public String transferHello() {
        try {
            return helloAPI.transferHello().getHello();
        } catch (ServiceUnavailableException e) {
            return "Service je dole.";
        }
    }
}
