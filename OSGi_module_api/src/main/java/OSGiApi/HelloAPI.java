package OSGiApi;

import DataTransfer.DataTransfer;

/**
 * Toto je interface pre OSGi modul. Definuje sluzbu, ktoru nejaky modul poskytuje inym modulom.
 * Ine moduly si tuto sluzbu mozu kvasi injectnut pricom si vieme vybrat, ktoru implementaciu chceme
 * a vieme kedy dana implementacia nebezi. Interfaces existuju vo vlastnej kniznici, ktora je nasadena
 * na glassfish ako samostatny osgi modul.
 */
public interface HelloAPI {
    String hello();

    DataTransfer transferHello();
}
