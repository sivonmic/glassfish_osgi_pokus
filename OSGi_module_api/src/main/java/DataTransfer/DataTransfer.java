package DataTransfer;

public class DataTransfer {
    String hello;
    int foo;

    public DataTransfer(String hello) {
        this.hello = hello;
        foo = 42;
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

    public int getFoo() {
        return foo;
    }

    public void setFoo(int foo) {
        this.foo = foo;
    }
}
