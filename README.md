Toto je demonštračná appka pre OSGi na glassfish.
Definuje tri OSGi pluginy:
1. api 
1. implementovane api 
1. hlavna aplikacia

V aplikácií je mnoho vysvetľujúcich commentov, prečítajte si aj tie v pom súboroch.
Ak sa chcete dozvedieť čo je to OSGi tak sa pozrite sem https://sharepoint.cvut.cz/team/13393/CZM/analyst/_layouts/15/WopiFrame2.aspx?sourcedoc=%7BAEF4B7F3-CCC7-4EB3-BFD6-EBBA902A48AF%7D&file=Architektura.docx&action=default&CT=1601296415738&OR=DocLibClassicUI

**Požiadavky**
- payara 5.2020 - potrebujete novú payaru kvôli java 11 link: https://cta-redirect.hubspot.com/cta/redirect/334594/7cca6202-06a3-4c29-aee0-ca58af60528a
- jdk 11: https://www.oracle.com/java/technologies/javase-jdk11-downloads.html

Payaru konfigurovať nie je treba kľudne to deploynite do defaultnej domény.

**Upozornenie o debug móde**

V intellij je bug, ktorý znemožňuje debugovanie lokálnej payary novších verzií. Preto na debugovanie musíte použiť remote konfiguráciu. Je to otravné ale java 11 bez toho nejde. Prítomnosť bugu v intellij potvrdená od JZ.
Pridajte si remote glassfish v intellij. Dôležité je v záložke startup/connection -> debug nastaviť debug port. Default pre payaru je 9009.
Potom zapnete payaru cez command line pomocou `./asadmin start-domain --debug meno_domeny` a keď sa zapne sa pripojíte cez intellij a môžte debugovať.

**Zapnutie**

1. Import do intellij ako maven projekt.
1. Vytvor textový súbor `password.properties` napríklad v koreňovom adresári payary. Tento súbor obsahuje `AS_ADMIN_PASSWORD=admin` kde napíšete miesto "admin" svoje payara heslo (moje heslo je "admin", áno)
1. V koreňovom pom zmeňte properties podľa vášho payara serveru a zahrňte cestu ku `password.properties` ktorý ste vytvorili v predchádzajúcom kroku.
1. Zapnite payara server cez command line ako popísané vyššie.
1. Pripojte intellij debug (ak chcete nie je nutné)
1. Zavolajte maven install pre koreňové pom. (Z intellij, v tej bočnej lište pre maven)
1. Appka sedí na http://localhost:8080/osgi_hello/api/hello zmeňte port ak ho máte v payare nastavený inak.

**Predvedenie**

*Upozornenie: Nikdy nemeňte run-time modul, ktorý obsahuje interfaces teda OSGi_module_api. Je možné meniť runtime OSGi_module_implemented a OSGi_webapp ale v prípade zmeny OSGi_module_api runtime to vybuchne. Ak sa vám to podarí takto vybuchnúť tak vypnite payaru. Zmažte applications/_internal osgi-cache a generated vo vašej doméne. Pri novšej payare to možno nie je nutné ale pri 5.192 som to musel takto mazať keď som to rozhasil.*

1. Choďte do payara admin consoly. V liste nasadených aplikácií undeploynite modul api_implemented. Skúste http://localhost:8080/osgi_hello/api/hello dostanete upzornenie že osgi modul api nie je k dispozícií. 
1. Potom zavolajte maven install pre modul api_implemented čo ho znovu nasadí. Opäť skúste  http://localhost:8080/osgi_hello/api/hello. Zase to funguje.
1. Skúste zmeniť správu ktorú to píše a zase redeploynite api_implemented modul. Správa sa zmení bez toho aby bolo nutné redeploynúť celú appku.

